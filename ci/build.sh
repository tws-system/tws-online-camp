#!/usr/bin/env sh

set -e

mkdir -p build-web/build
cp -R web/*  build-web/build

cp ci/Dockerfile build-web/Dockerfile
cp ci/default build-web/default
